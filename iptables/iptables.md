# Configurations
All configurations includes:
* INPUT DROP
* SSH port on 22.
* SMTP port 25 as `--reject-with icmp-port-unreachable`

- [ssh-only](iptables-ssh-only.fw) -> SSH
- [http-full](iptables-http-full.fw) -> HTTP/ HTTPS/ SMTPS
- [http-full-f2b](iptables-http-full-f2b.fw) -> HTTP/ HTTPS/ SMTPS/ fail2ban

## Usage

Simply:
```bash
iptables-restore < file.fw
```
