# Spacemacs configuration

![spacemacs example](spacemacs.png "Emacs with this Spacemacs configuration")

# Quick setup

1. Copy the `spacemacs.conf` file in your $HOME directory and rename as `.spacemacs`
2. Adapt the file for your needs
3. Install dependecies, for each layer in `dotspacemacs-configuration-layers` function following respective documentation, starting here: [spacemacs layers](https://github.com/syl20bnr/spacemacs/tree/develop/layers)
4. Install your font; in this conf we are using "Anonymous Pro for Powerline"
5. Execute this:
```bash
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
```


## Extra features

* https Elpa enabled
* Base16 Light Theme
* xclip is working
* arrow as powerline separator
* automatic trailing whitespaces deletion


## Power features

* Nyan cat scroll bar
