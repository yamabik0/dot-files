# dot-files

Unitoo standardized dotfiles.

## Directory Logic

The directory tree logic is the following: `program_name/program_name.conf` \
Each conf file can have
 an associated Markdown file named `program_name/program_name.md`

You can also put the conf inside the actual path
 of the configuration file following this logic: `program_name/path/to/conf/program_name.conf` \
\
**Examples:**  
- `neovim/neovim.conf`
- `nginx/etc/nginx/nginx.conf`
- `nginx/etc/nginx/nginx.md`

## Syntax highlighting

In order to display the proper syntax highlighting on Gitlab,
edit the .gitattributes file accordingly:

```txt
neovim/*.conf gitlab-language=vim
emacs/*.conf gitlab-language=elisp
```

## Support

<a href="https://liberapay.com/Unitoo/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

---

> [unitoo.it](https://www.unitoo.it) &nbsp;&middot;&nbsp;
> Mastodon [@unitoo](https://mastodon.uno/@unitoo) &nbsp;&middot;&nbsp;
> GitHub [@UnitooTeam](https://github.com/UnitooTeam)
